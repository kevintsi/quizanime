#include "functions.h"
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <string>
#include "json.hpp"

using namespace std;
using json = nlohmann::json;

void endLine()
{
    cout << endl;
}

void clearConsole()
{
    int i(0);
    while(i<2)
    {
        endLine();
        i++;
    }
}

void addBar()
{
    cout << "+----------------------------------------------+" << endl;
}

void tab(int index)
{
    while(index > 0)
    {
        cout << " ";
        index--;
    }
}

void title()
{
    addBar();
    tab(18);
    cout << "QuizAnime" << endl;
    addBar();
}

void checkResponse(int& response)
{
    while(response != 1 && response != 2)
    {
        endLine();
        cout << "Reponse invalide, reessayez." << endl;
        endLine();
        cout << "Saisissez le numero correspondant a votre reponse : ";
        response = 0;
        cin >> response;
    }
}

/**
 * Fonction permettant d'afficher le menu principal
 */
void menu(json& j, int& choice_anime)
{
    endLine();
    addBar();
    cout << "Choisis un anime parmi la liste suivante : " << endl;

    for(unsigned int i=0; i < j.size(); i++)
        cout << "[" << i + 1 << "] " << (string)j[i]["titre"] <<" "<<"("<<(string)j[i]["difficulte"]<<")"<< endl;

    addBar();
    endLine();
    cout << "Saisissez le numero correspondant a votre reponse : ";
    cin >> choice_anime;
    cin.ignore();

    while(choice_anime < 0 || choice_anime > j.size())
    {
        endLine();
        cout << "Reponse invalide, reessayez." << endl;
        endLine();
        cout << "Saisissez le numero correspondant a votre reponse : ";
        cin >> choice_anime;
        cin.ignore();
    }
}

/**
 * Fonction permettant d'afficher le quiz choisi
 */
bool quiz(json& j, int& choice_anime, int& nb_try, bool quizz_passed, int& index_questions, string& answer, string& good_answer, int& response, bool isStart, string player_name)
{
    for(int i = 0; i < j.size(); i++)
    {
        if(choice_anime == (int)j[i]["id"])
        {
            clearConsole();
            addBar();
            tab(16);
            cout << "Quiz " << (string)j[i]["titre"] << endl;
            addBar();

            endLine();
            endLine();
            endLine();
            tab(4);
            cout << "Nombre d'essais restants : " << nb_try << endl;
            endLine();
            addBar();

            while(nb_try > 0 && !quizz_passed)
            {
                bool test = nb_try > 0 || !quizz_passed;
                endLine();
                tab(16);
                cout << "Question " << index_questions + 1 << " :" << endl;
                endLine();
                tab(4);
                cout << (string)j[i]["questions"][index_questions]["question"] << endl;
                endLine();
                addBar();
                endLine();
                cout << "Saisissez votre reponse : ";
                getline(cin, answer);
                endLine();

                // Permet de convertir answer en lowercases
                for_each(answer.begin(), answer.end(), [](char & c) {
                    c = ::toupper(c);
                });

                good_answer = (string)j[i]["questions"][index_questions]["reponse"];

                // Permet de convertir good_answer en lowercases
                for_each(good_answer.begin(), good_answer.end(), [](char & c) {
                    c = ::toupper(c);
                });

                clearConsole();
                addBar();
                tab(16);
                cout << "Quiz " << (string)j[i]["titre"] << endl;
                addBar();
                endLine();

                // Compare la r�ponse saisie avec la r�ponse attendue (en lowercases)
                if(answer == good_answer)
                {
                    tab(4);
                    cout << "Bonne reponse !" << endl;
                    endLine();
                }
                else
                {
                    nb_try--;
                    tab(4);
                    cout << "Mauvaise reponse." << endl;
                    tab(4);
                    cout << "La bonne reponse etait : " << (string)j[i]["questions"][index_questions]["reponse"] << endl;
                }
                tab(4);
                cout << "Nombre d'essais restants : " << nb_try << endl;
                endLine();
                addBar();

                quizz_passed = index_questions == j[i]["questions"].size() - 1;
                index_questions++;
            }
            endLine();
            if(nb_try == 0)
            {
                tab(18);
                cout << "GAME OVER" << endl;
                quizz_passed == false;
            }
            if(quizz_passed)
            {
                tab(10);
                cout << "Felicitation, " << player_name << " !" << endl;
            }

            endLine();
            addBar();
            cout << "Voulez-vous jouer a un nouveau quiz ?" << endl;
            cout << "[1] Oui" << endl;
            cout << "[2] Non" << endl;
            addBar();
            endLine();
            cout << "Saisissez le numero correspondant a votre reponse : ";
            cin >> response;

            // Si la r�ponse saisie est diff�rente de 1 ou de 2, redemande une r�ponse
            checkResponse(response);

            nb_try = 3;
            if(response == 1)
            {
                clearConsole();
                title();
                endLine();
            }
            if(response == 2)
            {
                clearConsole();
                title();
                endLine();
                tab(12);
                cout << "Arret de QuizAnime..." << endl;
                endLine();
                addBar();
                endLine();
            }
        }
    }
    index_questions = 0;
    // Mettre la variable index_question � 0 permet de ne pas reprendre un quiz � la question o� celui-ci s'est arr�t�

    return isStart;
}
