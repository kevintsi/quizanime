# Quiz Anime
L'application 'Quiz Anime' permet à celui qui ose, de tester ses connaissances vis-à-vis de ses animes préférés. Les créateurs de cette application sont: DUBOST Maxime, TSI Kevin, ULCE Christ.


## Composition de l'application 
- Un fichier JSON où les animes (nom, questions, réponses) sont répertoriés 
- Des fonctions stocker dans des fichers distincts
- Et bien sûr le main.cpp regroupe les fonctionnalités 


## Règles du jeu 
- 3 vies 
- 5 questions par animés
- Et vos connaissances d'otaku qui font le reste


## Cas pratique (ce à quoi vous devez vous attendre visuellement)

Au lancement de l'application nous vous demanderons de vous identifier:

![](samplesIMG/debutquiz.PNG)


Ensuite, nous quantifiront votre nen(HxH), avec un question simple :

![](samplesIMG/otakuornot.PNG)



![](samplesIMG/questions.PNG)


Si vous êtes un vrai otaku et que vous avez survecu au questionnaire:


![](samplesIMG/win_tryagain.PNG)


Cepandant si ce n'est pas le cas:


![](samplesIMG/over_tryagain.PNG)




## Explication de nos choix
Pourquoi Quiz Anime?
+ Nous sommes à propos de la japanimation
+ #spotted tee-shirts ( ͡° ͜ʖ ͡°)

Pourquoi avoir utilisé un fichier JSON? 
+ Nous étions à l'aise avec la manipulation de ce type de fichier.
+ Rapide à créer
+ Facile à exploiter (ref ressources exploitées)
1.
![](samplesIMG/libraryjson.PNG)
2.
![](samplesIMG/usingjson.PNG)
3.
![](samplesIMG/ifstream.PNG)

+ Par choix purement esthétique.


## Perspectives d'amélioration
+ Questionnaire à choix multiple
+ Interface graphique
    Cas pratique: Un utilisateur aimerait tester ses connaissances vis-à-vis d'un animé.
    Mais ce dernier as un trou de mémoire lors du choix, il pourrait alors s'en remettre aux miniatures.
+ Réaliser l'application en orienté objet
    Cela aurait permis une factorisation du code, 
    une meilleure lisibilité aussi en console qu'en graphique.

## Resources

`Librairie JSON for modern C++`:https://github.com/nlohmann/json



