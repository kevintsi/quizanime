#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

#include <iostream>
#include <string>
#include "json.hpp"

using namespace std;
using json = nlohmann::json;

void endLine();

void clearConsole();

void addBar();

void tab(int index);

void title();

void checkResponse(int& response);
/**
 * Fonction permettant d'afficher le menu principal
 */
void menu(json& j, int& choice_anime);
/**
 * Fonction permettant d'afficher le quiz choisi
 */
bool quiz(json& j, int& choice_anime, int& nb_try, bool quizz_passed, int& index_questions, string& answer, string& good_answer, int& response, bool isStart, string player_name);
#endif // FUNCTIONS_H_INCLUDED
