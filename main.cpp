#include <iostream>
#include <fstream>
#include <typeinfo>
#include <string>
#include "json.hpp"
#include "functions.h"

using namespace std;
using json = nlohmann::json;

/**
 * Fonction principale
 */
int main()
{
    string player_name,
       answer,
       good_answer;

    int response = 0,
        choice_anime,
        nb_try = 3,
        index_questions = 0;

    bool quizz_passed = false,
         isStart = true;

    ifstream f("animeList.json", ifstream::binary);
    json j;
    f>>j;

    clearConsole();

    if(isStart)
    {
        addBar();
        tab(10);
        cout << "Bienvenue sur QuizAnime !" << endl;
        addBar();

        endLine();
        cout << "Saisissez votre nom : ";
        getline(cin,player_name);

        clearConsole();
        title();

        endLine();
        cout << "Bienvenue sur QuizAnime, " << player_name << " !" << endl;
        endLine();
        addBar();
        cout << "Etes-vous vraiment un(e) Otaku ? " << endl;
        cout << "[1] Oui" << endl;
        cout << "[2] Non" << endl;
        addBar();
        endLine();
        cout << "Saisissez le numero correspondant a votre reponse : ";
        response = 0;
        cin >> response;

        // Si la r�ponse saisie est diff�rente de 1 ou de 2, redemande une r�ponse
        checkResponse(response);
    }
    else
        response = 1;
        // Permet de retourner directement dans le "case 1", qui redirigera vers le menu principal

    switch(response)
    {
        case 1:
            if(isStart)
            {
                cin.ignore();

                clearConsole();
                title();

                endLine();
                cout << "Humm, c'est ce que l'on va voir, jeune kouhai..." << endl;

                isStart = false;
                // Permet de ne plus passer par les messages d'introduction
                // apr�s avoir termin� un quiz et s�lectionn� "rejouer"
            }
            else
            {
                title();
                endLine();
                tab(16);
                cout << "Menu principal" << endl;
            }


            // Affiche le menu principal
            menu(j, choice_anime);

            // Affiche le quiz et met � jour la valeur de isStart pour ne pas repasser par l'introdution
            // (isStart est retourn� par la fonction car il n'est pas possible de passer un bool par r�f�rence)
            isStart = quiz(j, choice_anime, nb_try, quizz_passed, index_questions, answer, good_answer, response, isStart, player_name);

            // Si le joueur ne souhaite pas relancer un quiz, fermeture du jeu
            if(response == 2)
                return 0;

            break;

        case 2:
            clearConsole();
            title();
            endLine();
            cout << "TU N'ES PAS DIGNE DE CE QUIZ !" << endl;
            cout << "REVIENS PLUS TARD RIAJUU." << endl;
            endLine();
            addBar();
            endLine();
            tab(12);
            cout << "Arret de QuizAnime..." << endl;
            endLine();
            addBar();
            endLine();
            // Quitte le jeu
            return 0;
    }

    // R�affiche le menu principal si je joueur souhaite lancer un nouveau quiz
    main();

    return 0;
}
